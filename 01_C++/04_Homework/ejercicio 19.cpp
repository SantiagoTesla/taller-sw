#include <iostream>
#include <stdlib.h>  //BIBLIOTECA DONDE ESTA EL VALOR ABSOLUTO
#include <math.h>  //BIBLIOTECA MATEMATICA 

//CALCULAREMOS EL PERIMETRO Y AREA DE UN TRIANGULO CON  SUS COORDENADAS
//PARA ELLO DEFINIREMOS LAS COORDENADAS DE CADA VERTICE DEL TRIANGULO
/*PARA HALLAR EL PERIMETRO Y AREA DEL TRIANGULO USAREMOS LAS SGTES FORMULAS:
AREA = 1/2* |X1*(Y2 - Y3) + X2*(Y3 - Y1) + X3(Y1 - Y2)|
PERIMETRO = ( (X3-X1)^2 + (Y3-Y1)^2 )^1/2  +  (X3-X2)^2 + (Y3-Y2)^2 )^1/2  +  (X2-X1)^2 + (Y2-Y1)^2 )^1/2 )
*/

using namespace std;
int main(void){
	
	//Declaration y initialize
	double X1, Y1, X2 ,Y2 ,X3 ,Y3, perimetro, area = 0;
	
	//Display phrase 1
	cout<<"Ingrese la coordenada en X del punto 1: " <<endl;
	cin>> X1;
	
	cout<<"Ingrese la coordenada en Y del punto 1: " <<endl;
	cin>> Y1;
	
	cout<<"Ingrese la coordenada en X del punto 2: " <<endl;
	cin>> X2;
	
	cout<<"Ingrese la coordenada en Y del punto 2: " <<endl;
	cin>> Y2;
	
	cout<<"Ingrese la coordenada en X del punto 3: " <<endl;
	cin>> X3;
	
	cout<<"Ingrese la coordenada en Y del punto 3: " <<endl;
	cin>> Y3;
	
	//Operation
	perimetro = sqrt( pow((X3-X1),2) + pow((Y3-Y1),2) ) + sqrt( pow((X3-X2),2) + pow((Y3-Y2),2) ) + sqrt( pow((X2-X1),2) + pow((Y2-Y1),2) );
	area = (abs((X1 * (Y2 - Y3)) +  (X2 * (Y3 - Y1))  +    (X3 * (Y1 - Y2)) ))/2;        
	
	//Display phrase 2
	cout<<"El Perimetro del triangulo es: "<<perimetro<<endl;
	cout<<"El Area del triangulo es: "<<area;
	
return 0;
}
