/**
 * @file Exercise 12
 * @santiago sergio 
 * @brief Un tonel es un recipiente, generalmente de madera, muy utilizado para almacenar y mejorar un vino. La forma de un tonel es muy 
 caracter�stica y es un cilindro en el que la parte central es m�s gruesa, es decir, tiene un di�metro mayor que los extremos. 
 Escriba un programa que lea las medidas de un tonel y nos devuelva su capacidad, teniendo en cuenta que el volumen (V) de un tonel
  viene dado por la siguiente f�rmula: V = PI l a2 donde:
 l = la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)
d  =  di�metro del tonel en sus extremos.
D  = di�metro del tonel en el centro: D>d
Nota: Observe que si las medidas se dan en cent�metros el resultado lo obtenemos en cent�metros c�bicos.

 * @date 27.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

#define PI 	3.1416	//Constante PI


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	//Declaration and initialization
	float height         = 0.0; //Altura del tonel
	float largerDiameter = 0.0; //Diametro mayor del centro del tonel
	float minorDiamater  = 0.0; //Diametro menor de sus extremos del tonel
	float volume         = 0.0; //volumen = capacidad del tonel
	
	//Display phrase 1
	cout<<"========== Insert Data ==========\r\n";
	cout <<"Digite la altura : "; 
	cin>>height ;                                      //leer la altura
	cout<<"\r\nDigite el diametro  mayor en cm : ";
	cin>>largerDiameter ;                              //leer el diametro mayor
	cout<<"\r\nDigite el diametro menor en cm : ";
	cin>>minorDiamater ;                               //leer el diametro menor
	
	
	//Calculation
	volume = (PI/12)*(height)*(2*(pow(largerDiameter,2))+pow(minorDiamater,2)) ; //Formula para hallar el volumen del tonel con la altura y los diametros de sus extremos y  del centro
	
	//results
	cout<<"\r\n========== Show results ==========\r\n";
	cout<<"El volumen del tonel es : "<<volume<<"cm� " ;  //Resultado de la capacidad del tonel

	return 0 ;
	
}
	
	
	
	
