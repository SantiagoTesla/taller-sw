/*******************Include*******************/
#include<iostream>
#include<math.h>
using namespace std;
const float G=0.00000006673;
/*
Excercise Number 7
*/
/************Function Declaration************/
void Run();
void Collectdata();
void Calculate();
void ShowResults();
/******************Main**********************/
int main(){
	Run();
	return 0;
}

/*************Global Variable****************/
	
	double F;
	double M1;
	double M2;
	double D;
	double d;  
/************Function Definition*************/
void Run(){
	Collectdata();
	Calculate();
	ShowResults();
}
//============================================//
void Collectdata(){
	cout<<"Ingrese el valor de la masa del cuerpo 1 (en Kg) : "<<endl;
	cin>>M1;
	cout<<"Ingrese el valor de la masa del cuerpo 2 (en Kg) : "<<endl;
	cin>>M2;
	cout<<"Ingrese el valor de la distancia entre los cuerpos (en m) : "<<endl;
	cin>>D;
}
//============================================//
void Calculate(){
	d= pow(D,2);
	//Puse por mil al producto de G , M1 y M2 porque en el problema dice que est� en cm3/g.seg2	
	F=(G*M1*M2*1000)/d;
}
//============================================//
void ShowResults(){
	cout<<"La Fuerza gravitacional entre los dos cuerpos es : "<<F<<endl;
}
	

