/*******************Include*******************/
#include<iostream>
using namespace std;
/*
Excercise Number 6
*/
/************Function Declaration************/
void Run();
void Collectdata();
void Calculate();
void ShowResults();
/******************Main**********************/
int main(){
	Run();
	return 0;
}
/*************Global Variable****************/
    int T;
	int S;
	int D;
	int H;
	int M;
	int d;
	int h;
	int m;
/************Function Definition*************/
void Run(){
	Collectdata();
	Calculate();
	ShowResults();
}
//============================================//
void Collectdata(){
	cout<<"Escriba la cantidad total de segundos : "<<endl;
	cin>>T;
}
//============================================//
void Calculate(){
	D=T/86400;
	d=T%86400;
	H=d/3600;
    h=d%3600;
    M=h/60;
    m=h%60;
    S=m/60;
}
//============================================//
void ShowResults(){
	cout<<"Cantidad de dias : "<<D<<endl;
	cout<<"\n\rCantidad de horas : "<<H<<endl;
	cout<<"\n\rCantidad de minutos : "<<M<<endl;
	cout<<"\n\rCantidad de segundos : "<<S<<ednl;
}


