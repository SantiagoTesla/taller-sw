/*******************Include*******************/
#include<iostream>
#include<math.h>
using namespace std;
/*
Excercise Number 8
*/
/************Function Declaration************/
void Run();
void Collectdata();
void Calculate();
void ShowResults();
/******************Main**********************/
int main(){
	Run();
	return 0;
}
/*************Global Variable****************/
    double C;
	double i;
	double n;
	double c;
	double t;
	double M;
	double I;
	double e;
	double P;
	double p;
	double pc;
	double pi;
/************Function Definition*************/
void Run(){
	Collectdata();
	Calculate();
	ShowResults();
}
//============================================//
void Collectdata(){
	cout<<"Ingrese el capital que sera depositado (en s/.) : "<<endl;
	cin>>C;
	cout<<"Ingrese la tasa de interes (en %) : "<<endl;
	cin>>i;
	cout<<"Ingrese el periodo del interes (en meses) : "<<endl;
	cin>>n;
	cout<<"Ingrese que tan capitalizable es (mestralmente) : "<<endl;
	cin>>c;
	cout<<"Ingrese el tiempo que durara el interes (en meses) : "<<endl;
	cin>>t;
}
//============================================//
void Calculate(){
	e=t/c;
	p=i/100;
	pi=n/c;
	pc=p/pi;
	P=pow(1+pc,e);
	M=C*P;
	I=M-C;
}
//============================================//
void ShowResults(){
	cout<<"El monto compuesto acumulado es de : "<<M<<endl;
	cout<<"\n\rEl interes compuesto ganado es de : "<<I<<endl;
}



