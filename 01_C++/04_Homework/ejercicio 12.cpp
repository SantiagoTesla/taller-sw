/**
 
 * @santiago sergio
 * @brief Escriba un programa para calcular el tiempo transcurrido,
   en minutos, necesario para hacer un viaje. La ecuaci�n es tiempo transcurrido =
   distancia total/velocidad promedio. Suponga que la distancia est� en kil�metros 
   y la velocidad en kil�metros/hora. 
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/




/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	//Declaration and initialization
	float kilometer   =  0.0;       //distancia en kilometro
	float timeElapsed =  0.0;       //Tiempo transcurrido
	float averageSpeed=  0.0;       //Velocidad promedio 
	
	
	//Display phrase 1
	cout<<"========== Insert Data ==========\r\n";
	cout <<"Escriba la velocidad promedio en km/s : "; 
	cin>>averageSpeed ;                                 //leer la velocidad promedio
	cout<<"\r\nEscriba la distancia total en Km : ";
	cin>> kilometer;                                    //leer la distancia total


	//Calculation
	timeElapsed = (kilometer)/(averageSpeed*60);    //El tiempo transcurrido en minutos es igual al kilometro dividido todo por la velocidad * 60

	
	//Results
	cout<<"\r\n========== Show results ==========\r\n";
	cout<<"Su tiempo transcurrido en minutos es : "<<timeElapsed;   //Mostrando los resultados del problema
	
	
	return 0 ;
	
}


	
	

