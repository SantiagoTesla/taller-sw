
#include <iostream>

using namespace std;

//Use of the function int main

int main(){
	
	
	//Declaration of the variables
	
	float R; //R: Cilinder base radius
	
	float H; //H: Cilinder height
	
	float LateralArea; // Cilinder lateral area
	
	float Volume; // Cilinder volume
	
	
	//Initialization of the variables
	
	R = 1;
	H= 1;
	LateralArea=1;
	Volume=1;
	
	//Dislplay phrase 1
	
	cout<<"Digite el radio de la base del cilindro: ";
	
	cin>>R;
	
	cout<<"Digite la altura del cilindro: ";
	
	cin>>H;
	
	//Operations
	
	LateralArea = 2*(3.14)*R*H;
	Volume = (3.14)*R*R*H;
	
	//Display phrase 2
	
	cout<<"\r\nEl area lateral del cilindro es: " <<LateralArea;
	cout<<"\r\nEl volumen del cilindro es: " <<Volume;
	
	
	
	return 0;
	

	
}



