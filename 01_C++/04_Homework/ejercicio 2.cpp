
 /**
 * @file codeReview.cpp
 * @santiago sergio
 * @date 05.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/



int main(){
	
	int numberMen = 0;
	int numberWomen = 0;
	int porcentageMen = 0.0;
	int porcentageWomen = 0.0;
	
	cout<<"Ingrese el numero de varones: ";
	cin>>numberMen;
	cout<<"\n\rIngrese el numero de mujeres: ";
	cin>>numberWomen;
	
	porcentageMen = ( (float)numberMen/((float)numberMen + (float)numberWomen) ) * 100.0;
	porcentageWomen = ( (float)numberWomen/((float)numberMen + (float)numberWomen) ) * 100.0;
	
	cout<<"\n\rel porcentaje de varones es :"<<porcentageMen<<"%";
	cout<<"\n\rel porcentaje de mujeres es :"<<porcentageWomen<<"%";
	
	return 0;
}
